////doesnt allow for arrow keys and space bar to move the screen
window.addEventListener("keydown", function(e) {
    if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
        e.preventDefault();
    }
}, false);




//========================VARIBALBES BELOW=======================//
//ids from html
const canvas = document.getElementById("canvas");
const context = canvas.getContext("2d");

let startTXT = document.getElementById("start");

//gamer input
let gamerInput = new GamerInput("None"); 

//lets - things needed down the line
let start = 0;
let frames = 4;
let currentFrame = 0; //used for animations
let facing = 0; // 0 = forward, 1= left, 2 = right, 3=back
let idle =4; // 0 = moving, 4 = idle, (4 because there's 4 directions)
let scene = 0; //controls the scenes and whats drawn
let sceneName = "Starting Out";
let action = 1; //starts at 1, to start the dialogue and stuff
let playerIsIdle = 0; //variable for the idle timer
let enableMouse = ""; //enables the nipple.js
let mouseEnabledString = ""; //displays if mouse is enabled
let username = "";

let max = 100;
let counter = 5;
let timeout = null;

//starting position, can be adjust later uwu
let xpostion = 200;
let ypostion = 50;

//time
let initial = new Date().getTime();
let current; 

//images
let playerIMG = new Image();
let frenIMG = new Image();
let bgIMG = new Image();
let overlayIMG = new Image();
let treeIMG = new Image ();
let frenTalkIMG = new Image();
let textBoxIMG = new Image();

//sprites - sources
playerIMG.src = "assets/media/america.png";
frenIMG.src = "assets/media/canada.png";
bgIMG.src = "assets/media/background.png";
overlayIMG.src = "assets/media/overlay1.png";
treeIMG.src = "assets/media/tree.png";
frenTalkIMG.src = "assets/media/canadatalk.png";
textBoxIMG.src = "assets/media/textbox.png";

//local storage
let form = document.forms["helloForm"];
let modal = document.getElementById("modal");
let modalContent = modal.children[0].children[2];
let validateButton = document.getElementsByClassName("saved-data-accept")[0];
let dismissButton = document.getElementsByClassName("saved-data-refusal")[0];

//========================GAME FUNCTIONS BELOW=======================//


if(typeof(Storage) !== "undefined") {
    // console.log("Local storage is supported.");
    // Local storage is available on your browser
    username = localStorage.getItem('username');
    let scene = localStorage.getItem('scene');
    let action = localStorage.getItem('action');
    if (username){
        form.style.display = "none";
        modal.style.display = "block";
        modalContent.innerHTML = "username: " + username + "<br>" + "scene: " + scene + "<br>" + "action: " + action;
        validateButton.onclick = function(){
            modal.style.display = "none";
            form.style.display = "none";
        }
        dismissButton.onclick = function(){
            modal.style.display = "none";
            form.style.display = "block";
            //the following is not necessary in this case, but I'll leave it here in case you need it later
            localStorage.clear();
        }
    }
    else{
        console.log("no data in localStorage, loading new session")
    }
  } else {
    console.log("Local storage is not supported.");
    // The condition isn't met, meaning local storage isn't supported
  }

// Stores the item data



function validateForm(){
    event.preventDefault();
    var x = document.forms["helloForm"]["name"].value;
    if (x == "") {
        alert("I need to know your name so I can say Hello");
        return false;
    }
    else{
        alert("Hello there " + document.forms["helloForm"]["name"].value);
        localStorage.setItem("username", x);
        start = 1;
        modal.style.display = "none";
        form.style.display = "none";
        //more advanced pt2: make a system that changes the webpage based on the inputted name 
    }
}


function gameLoop(){
    //these functions gotta be outside the if, otherwise you cant start at all
    startGame();
    pauseGame();
    if (start == 1){ //only start playing when the player is ready
        draw();
        update();
    }

    requestAnimationFrame(gameLoop);
}

//updates other functions
function update(){
    playerMVMT();
    sceneChanger();
    if (scene === 0 || scene === 2)   
    {
        idleTimer();
    }
}

//draws the appropriate scene
function draw(){
    if (scene == 0)
    {
        scene0();
        chapterEval()
    }
    if (scene == 1)
    {
        scene1();
        chapterEval()
        sceneName = "Hey You..";
    }
    if (scene == 2)
    {
        scene2();
        chapterEval()
        sceneName = "Exploration Time!";
    }     
}

//allows for scene changes by canvas positions, text changes are within it's own fucntion
function sceneChanger(){
    if (scene == 0){
        if (xpostion >= 470 && ypostion >= 230 && xpostion <= 540 && ypostion <= 330){
            scene = 1;
            console.log("scene change");
        }
    }
}


//========================PLAYER-BASED FUNCTIONS BELOW=======================//

//player movement as well as idle setter
function playerMVMT()
{
    //only allows the player tio jump when they havent jumped
    if (gamerInput.action === "Up")
    {
        facing = 3;
        ypostion -= 1;
        idle = 0;

    }
    if (gamerInput.action === "Down")
    {
        facing = 0;
        ypostion += 1;
        idle = 0;

    }
    if (gamerInput.action === "Left")
    {
        facing = 1;
        xpostion -= 1;
        idle = 0;

    }
    if (gamerInput.action === "Right")
    {
        facing = 2;
        xpostion += 1;
        idle = 0;

    }
    //if not movement is detected, become idle
    if (gamerInput.action === "None")
    {
        idle = 4;
    }

    if (gamerInput.action === "MouseOn")
    {
            enableMouse = "on";
            mouseEnabledString = "Mouse Enabled";

            //nipple.on('start move end dir plain', function (evt) {
            joystick.on('dir:up', function (evt, data) 
            {
                gamerInput = new GamerInput("Up");
            });
            joystick.on('dir:down', function (evt, data) 
            {
                gamerInput = new GamerInput("Down");
            });
             joystick.on('dir:left', function (evt, data) 
            {
                gamerInput = new GamerInput("Left");
            });
            joystick.on('dir:right', function (evt, data) 
            {
                gamerInput = new GamerInput("Right");
            });

    }
    if (gamerInput.action === "MouseOff")
    {
            joystick.off();
            enableMouse = "off";
            mouseEnabledString = "Mouse Disabled";
    }

}

//mobile controls
function noMoving(){
    gamerInput = new GamerInput("None");
}

function pressUp(){
    gamerInput = new GamerInput("Up");
}
function pressDown(){
    gamerInput = new GamerInput("Down");
}
function pressLeft(){
    gamerInput = new GamerInput("Left");
}
function pressRight(){
    gamerInput = new GamerInput("Right");
}
function interactButt(){
    gamerInput = new GamerInput("Space");
}
function pauseButt(){
    gamerInput = new GamerInput("Pause");    
}
function startButt(){
    gamerInput = new GamerInput("Start");    
}
function mouseOn(){
    gamerInput = new GamerInput("MouseOn");
}
function mouseOff(){
    gamerInput = new GamerInput("MouseOff");
}



var joystick = nipplejs.create
({
    color: 'DarkMagenta',
    zone: document.getElementById('canvas'),
    mode: 'dynamic',
    position: {left: '50%', top: '50%'}
});


//desktop controls
function GamerInput(input){
    //input is passed from input functions, left, right etc.
    this.action = input;
}

function input(event){
    //listens for any keydown events, aka the input from the user.
    if (event.type === "keydown")
    {
        //accounts for the 4 directional keys, and a default of none.
        switch (event.keyCode) 
        {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break;
            case 65: // A
                gamerInput = new GamerInput("Left");
                break;

            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; 
            case 87: // W
                gamerInput = new GamerInput("Up");
                break;

            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break;
            case 68: // D
                gamerInput = new GamerInput("Right");
                break;

            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break;
            case 83: // S
                gamerInput = new GamerInput("Down");
                break;

            case 13: //enter
                gamerInput = new GamerInput("Start");        
                break;
            
            case 80: //p
                gamerInput = new GamerInput("Pause");        
                break;

            case 32: //space
                gamerInput = new GamerInput("Space");        
                break;

            case 69: // e
                gamerInput = new GamerInput("MouseOn");        
                break;
            case 81: //q
                gamerInput = new GamerInput("MouseOff");        
                break;
            }
 
                           

            

        }

        //if no key is pressed, do nothing.
        else
        {
            gamerInput = new GamerInput("None");
        }
}

//starts the game
function startGame(){
    if (gamerInput.action === "Start")
    {
        start = 1;
        startTXT.innerHTML = " ";
    }
}
//pauses the game
function pauseGame(){
    if (gamerInput.action === "Pause")
    {
        start = 0;
    }
}

function idleTimer(){

    //if the action is none, start a time out for 3 seconds (temp)
    if (gamerInput.action === "None")
    {
        timeout = setTimeout(idler, 10000);
            function idler() 
            {
                playerIsIdle = 1;
            }
    }
    //otherwise clear it and set everything as its supposed to be
    else
    {
        clearTimeout(timeout);
        playerIsIdle = 0;
        counter = 0;
    }

    //if the player is officially idle, start drawing the bar
    if (playerIsIdle === 1)
    {
        counter += 0.1;

        // Draw the background
        context.fillStyle = "#FFFFFF";
        context.fillRect(0, 0, canvas.width, 32);
        context.fillStyle = "#000000 ";
        context.fillRect(0, 0, canvas.width, 30);
        context.fillStyle = "#FFFFFF";
        context.textAlign = "center";
        context.font = "26px Arial";
        context.fillText("Idle Timer", canvas.width/2, 24);
        
        // Draw the fill
        context.fillStyle = "#faff52";
        var fillVal = Math.min(Math.max(counter / max, 0), 1);
        context.fillRect(0, 0, fillVal * canvas.width, 30);

        if (counter >= max)
        {
            gamerInput = new GamerInput("Pause"); 
        }

    }
}

function chapterEval()
{
    document.getElementById("progress").innerHTML = "Chapter: " + sceneName + "<br>" + mouseEnabledString;
}


//========================SCENES BELOW=======================//

//the og scene, draws the walking to other character
function scene0(){
    current = new Date().getTime();
    if (current - initial >= 450) {
        currentFrame = (currentFrame + 1) % frames;
        initial = current;
    }
    context.drawImage(bgIMG, 0,0,canvas.width, canvas.height);
    context.drawImage(frenIMG, (128 * currentFrame), 768 , 128, 200, 530, 250, 64, 100);
    context.drawImage(playerIMG, (128 * currentFrame), ((facing + idle) * 193), 128, 193, xpostion, ypostion, 64, 100);
    context.drawImage(treeIMG, (480 * currentFrame), 0, 480, 480, 520, 150, 200, 200); 
}

function scene1(){
    current = new Date().getTime();
    if (current - initial >= 500) {
        currentFrame = (currentFrame + 1) % frames;
        initial = current;
    }
    context.drawImage(bgIMG, 0,0,canvas.width, canvas.height);

    localStorage.setItem("scene", scene);
    localStorage.setItem("action", action);

    switch (action){

        case 1:     
        context.font = '70px Monospace';
        context.drawImage(frenIMG, (128 * currentFrame), 768 , 128, 200, 530, 250, 64, 100);
        context.drawImage(playerIMG, (128 * currentFrame), 1152, 128, 200, 490, 250, 64, 100);
        context.drawImage(treeIMG, (480 * currentFrame), 0, 480, 480, 520, 150, 200, 200);
        context.drawImage(overlayIMG, 0,0, canvas.width, canvas.height);  
        context.drawImage(textBoxIMG, 0, 550, canvas.width, 150);
        context.fillStyle = "#000000";
        context.textAlign = "left";
        context.fillText("AH!", 40, 620); 
        context.drawImage(frenTalkIMG, (600 * currentFrame), 2400, 600, 600, 650, 200, 500, 500);
        
        if (gamerInput.action === "Space")
        { 
            setTimeout(function() { 
                action = 2; 
            }, 300);
        }
        break;

        case 2:
        context.drawImage(frenIMG, (128 * currentFrame), 768 , 128, 200, 530, 250, 64, 100);
        context.drawImage(playerIMG, (128 * currentFrame), 1152, 128, 200, 490, 250, 64, 100);
        context.drawImage(treeIMG, (480 * currentFrame), 0, 480, 480, 520, 150, 200, 200);
        overlayIMG.src = "assets/media/overlay2.png"     
        context.drawImage(overlayIMG, 0,0, canvas.width, canvas.height);  
        context.drawImage(textBoxIMG, 0, 550, canvas.width, 150);     
        context.font = '24px Monospace';
        context.fillStyle = "#000000";
        context.textAlign = "left";
        context.fillText("Hey you", 40, 600); 
        context.drawImage(frenTalkIMG, (600 * currentFrame), 0, 600, 600, 650, 200, 500, 500);
        if (gamerInput.action === "Space")
        { 
            setTimeout(function() { 
                action = 3; 
            }, 300);
        }
        break;

        case 3:
            overlayIMG.src = "assets/media/overlay3.png"  
            context.drawImage(overlayIMG, 0,0, canvas.width, canvas.height);
            context.drawImage(textBoxIMG, 0, 550, canvas.width, 150);     
            context.font = '24px Monospace';
            context.fillStyle = "#000000";
            context.textAlign = "left";
            context.fillText("You're finally awake..", 40, 600);
            context.drawImage(frenTalkIMG, (600 * currentFrame), 1800, 600, 600, 650, 200, 500, 500); 
            if (gamerInput.action === "Space")
            { 
                setTimeout(function() { 
                    action = 4; 
                }, 300);
            }
            break;

            case 4:
                overlayIMG.src = "assets/media/overlay2.png"
                bgIMG.src = "assets/media/background2.png"; 
                context.drawImage(overlayIMG, 0,0, canvas.width, canvas.height);
                context.drawImage(textBoxIMG, 0, 550, canvas.width, 150);     
                context.font = '24px Monospace';
                context.fillStyle = "#000000";
                context.textAlign = "left";
                context.fillText("You were trying to cross the border.. right?", 40, 600); 
                if (gamerInput.action === "Space")
                { 
                    setTimeout(function() { 
                        action = 5; 
                    }, 300);
                }
                break;
                
            case 5:
                overlayIMG.src = "assets/media/overlay1.png" 
                context.drawImage(overlayIMG, 0,0, canvas.width, canvas.height);
                context.drawImage(textBoxIMG, 0, 550, canvas.width, 150);     
                context.font = '24px Monospace';
                context.fillStyle = "#000000";
                context.textAlign = "left";
                context.fillText("Walked right into that imperial ambush...", 40, 600); 
                if (gamerInput.action === "Space")
                { 
                    setTimeout(function() { 
                        action = 6; 
                    }, 300);
                }
                break;

            case 6:
                context.drawImage(textBoxIMG, 0, 550, canvas.width, 150);     
                context.font = '24px Monospace';
                context.fillStyle = "#000000";
                context.textAlign = "left";
                context.fillText("Same as us.. and that thief over there..", 40, 600); 
                if (gamerInput.action === "Space")
                { 
                    setTimeout(function() { 
                        action = 7; 
                    }, 300);
                }
                break;
            case 7:
                bgIMG.src = "assets/media/background.png"; 
                context.font = '70px Monospace';
                context.drawImage(bgIMG, 0,0,canvas.width, canvas.height);
                context.drawImage(frenIMG, (128 * currentFrame), 768 , 128, 200, 530, 250, 64, 100);
                context.drawImage(playerIMG, (128 * currentFrame), ((facing + idle) * 193), 128, 193, xpostion, ypostion, 64, 100);
                context.drawImage(treeIMG, (480 * currentFrame), 0, 480, 480, 520, 150, 200, 200); 

                scene = 2;
                xpostion = 490;
                ypostion = 250;


            break;

    }
}

function scene2()
{
    current = new Date().getTime();
    if (current - initial >= 450) {
        currentFrame = (currentFrame + 1) % frames;
        initial = current;
    }
    context.drawImage(bgIMG, 0,0,canvas.width, canvas.height);
    context.drawImage(frenIMG, (128 * currentFrame), 768 , 128, 200, 530, 250, 64, 100);
    context.drawImage(playerIMG, (128 * currentFrame), ((facing + idle) * 193), 128, 193, xpostion, ypostion, 64, 100);
    context.drawImage(treeIMG, (480 * currentFrame), 0, 480, 480, 520, 150, 200, 200); 
}



//========================DECLERATIONS BELOW=======================//

window.requestAnimationFrame(gameLoop);

window.addEventListener('keydown',input);
window.addEventListener('keyup',input);

